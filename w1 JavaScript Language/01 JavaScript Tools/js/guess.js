let targetNumber = Math.floor(Math.random() * 10) + 1;
let guesses = 0;

function init () {
    'use strict';
    console.log("Game starts");
    document.getElementById('form').addEventListener('submit', function(event){
        event.preventDefault();
        check(document.getElementById('input').value);
    });
    
}

function check (value) {
    if ( value == targetNumber ){
            showWin();
            window.location.reload();
    } if ( value != targetNumber ) {
            guesses++;
            console.log(guesses);
            showError();
    } if ( guesses == 5 ) {
            showLoss();
            guesses = 0;
            window.location.reload();
        }
}

function showWin () {
    var f = document.getElementById('form');
    f.parentNode.removeChild(f);
    document.getElementById('tulos').innerHTML = "You won";
}

function showError () {
    document.getElementById('tulos').innerHTML = "Incorrect";
}

function showLoss () {
    var f = document.getElementById('form');
    f.parentNode.removeChild(f);
    document.getElementById('tulos').innerHTML = "You lost";
}

init();
