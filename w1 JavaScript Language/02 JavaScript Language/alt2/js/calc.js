function getNumber(id) {
    return parseInt(document.getElementById(id).value);
}

function setResult(result) {
    document.getElementById("result").innerHTML = result;
}

function plus() {
    setResult(getNumber("firstValue") + getNumber("secondValue"));
}

function mult() {
    setResult(getNumber("firstValue") * getNumber("secondValue"));
}

function divide() {
    var num1 = getNumber("firstValue");
    var num2 = getNumber("secondValue");
    
    if ( num2 === 0 ){
        setResult("Can't divide with 0");
    } else {
        setResult(num1 / num2);
    }
}

function subst() {
    setResult(getNumber("firstValue") - getNumber("secondValue"));
}